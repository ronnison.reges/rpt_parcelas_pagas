********************************************************************************
120MQSPN0002
********************************************************************************
* ESTE PATCH CONTEM:
* =================
* 
* Gap 120MQSPN0002
* 
* 
* PR�-REQUISITOS:  
* ==============
* 
* N/A
* 
* INSTRUCOES P�S-PATCH:
* ====================
* 
* N/A
* 
********************************************************************************  

################################################################################
###                 Procedimentos a serem realizados:                        ###
################################################################################

1) Copiar o arquivo 120MQSPN0002.zip para o diret�rio $APPL_TOP/patches.
Caso n�o exista o diret�rio patches cri�-lo.

2) Executar:
 
        $ cd $APPL_TOP/patches
        $ unzip -o 120MQSPN0002.zip
        $ cd 120MQSPN0002
 
3) Aplicar atraves do "adpatch" os drivers contidos no patch
   Em alguns casos de patches n�o teremos todos os drivers
 
        Nome dos poss�veis drivers existentes para o patch
        - u12mqsPN0002.drv
 
        adpatch options=hotpatch driver=u12mqsPN0002.drv logfile=u12mqsPN0002.log patchtop=$PWD workers=1 defaultsfile=$APPL_TOP/admin/$TWO_TASK/defaults.txt
 
  Obs: Se o ambiente do projeto for 11.5.8 ou superior � necess�rio que
  o adpatch seja executado com a op��o "options=noprereq"